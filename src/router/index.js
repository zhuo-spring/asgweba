import VueRouter from "vue-router";
// 引入组件
import HomeView from '@/views/HomeView'
import Enroll from '@/views/Enroll'
import Team from '@/views/Team'
import Championship from '@/views/Championship'
import Thanks from '@/views/Thanks'
import Login from '@/views/Login'
import About from '@/views/About'
import Manager from '@/views/Manager'
import Schedule from '@/views/Schedule'
import Operation from '@/components/manager/Operation'
import Players from '@/components/manager/Players'
import SetGame from '@/components/manager/SetGame'
import Guessboard from '@/components/guess/Guessboard'
import MyGuess from '@/components/guess/Myguess'
import abloge from '@/components/blog/ablg'

const router = new VueRouter({
    routes: [
        {
            path: '/',
            component: HomeView,
            meta: { title: 'ASG官方网站' },
        },
        {
            path: '/blog/a',
            component: abloge,
            meta: { title: '一篇文章' },
        },
        {
            path: '/enroll',
            component: Enroll,
            meta: { title: '报名'},
        },
        {
            path: '/team',
            component: Team,
            meta: { title: '现报名上队伍' },
        },
        {
            path: '/championship',
            component: Championship,
            meta: { title: '历届冠军' },
        },
        {
            path: '/thanks',
            component: Thanks,
            meta: { title: '感谢' },
        },
        {
            path: '/login',
            component: Login,
            meta: { title: '登录' },
        },
        {
            path: '/manager',
            component: Manager,
            meta: { title: 'ASG赛事管理员' },
            children:[
                {
                    path:'operation',
                    component: Operation,
                    meta: { title: '管理员系统' },
                },
                {
                    path:'players',
                    component: Players,
                    meta: { title: '查看选手信息' },
                },
                {
                    path:'setgame',
                    component: SetGame,
                    meta: { title: '设置赛程' },
                }
            ]
        },
        {
            path: '/about',
            component: About,
            meta: { title: '关于ASG' },
        },
        {
            path: '/schedule',
            component:Schedule,
            meta: { title: '赛程' },
            children:[
                {
                    path:'guess',
                    component:Guessboard,
                    meta: { title:'竞猜'}
                },
                {
                    path:'myguess',
                    component:MyGuess,
                    meta: { title:'我的竞猜'}
                }
            ]
        }
    ]
});
router.afterEach((to)=>{
    document.title= to.matched[0].meta.title;
})

export default router