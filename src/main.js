import Vue from 'vue'
import App from './App.vue'
import router from './router'
// 引入路由
import VueRouter from 'vue-router'
// axios
import axios from 'axios'
import VueAxios from 'vue-axios'
// element
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// editor
import VMdEditor from '@kangc/v-md-editor'
import '@kangc/v-md-editor/lib/style/base-editor.css'
// preview
import VMdPreview from '@kangc/v-md-editor/lib/preview'
import '@kangc/v-md-editor/lib/style/preview.css'
import githubTheme from '@kangc/v-md-editor/lib/theme/github.js'
import '@kangc/v-md-editor/lib/theme/style/github.css'
import hljs from 'highlight.js'
import Varlet from '@varlet-vue2/ui'
import '@varlet-vue2/ui/es/style.js'
Vue.use(Varlet)

new Vue({
  render: h => h(App)
}).$mount('#app')

// 使用
VMdEditor.use(githubTheme, {
  Hljs: hljs
})
VMdPreview.use(githubTheme,{
  Hljs: hljs
})
// 默认提示
Vue.config.productionTip = false
//设置默认地址：
axios.defaults.baseURL = 'https://124.223.35.239';

Vue.use(VueAxios, axios);
Vue.use(VueRouter);
Vue.use(ElementUI);
Vue.use(VMdEditor);
Vue.use(VMdPreview)

new Vue({
  el: '#app',
  router,
  render: h => h(App),
})
